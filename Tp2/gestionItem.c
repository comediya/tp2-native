
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structureRepas.h"




void chargerMenu(menu *aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}


void afficherMenu(menu aMenu) {
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        printf("%i %s\n", i+1, aMenu.listeRepas[i]->titre);

    }
}





commandeItem *commanderItems(menu aMenu, int *nombreCommandes){
    int choixRepas, choixItem, retourMenu = 0;
    commandeItem *resultat = NULL;
    commandeItem *dernierCommande = NULL;




    printf("Bienvenue! Voici le menu:\n");
    int sortir = 1;

    while (sortir) {

        afficherMenu(aMenu);
        printf("0  retour\n");
        printf("\nVeuillez choisir le numéro du repas: ", aMenu.nombreRepas);
        scanf("%d", &choixRepas);

        if (choixRepas == 0) {
            sortir = 0;
            continue;
        }

        if (choixRepas >= 1 && choixRepas <= aMenu.nombreRepas) {
            repas *repasChoisi = aMenu.listeRepas[choixRepas - 1];

            while (!retourMenu) {


                printf("\nVoici les items du repas %s:\n", repasChoisi->titre);
                for (int i = 0; i < repasChoisi->nombreItem; i++) {
                    printf("%d %s %3.2f\n", i + 1, repasChoisi->listeItem[i].titre, repasChoisi->listeItem[i].prix);
                }
                printf("%d Retour\n", repasChoisi->nombreItem + 1);

                printf("\nVeuillez choisir le numéro de l'item : ");
                scanf("%d", &choixItem);

                if (choixItem == repasChoisi->nombreItem + 1) {
                    retourMenu = 1;
                } else if (choixItem >= 1 && choixItem <= repasChoisi->nombreItem) {
                    item *itemChoisi = &repasChoisi->listeItem[choixItem - 1];

                    commandeItem *copieCommande = calloc(1, sizeof(commandeItem));

                    strcpy(copieCommande->titre, itemChoisi->titre);

                    copieCommande->prix = itemChoisi->prix;
                    copieCommande->quantite = 1;
                    (*nombreCommandes)++;

                    if(resultat == NULL){
                        resultat = copieCommande;
                        dernierCommande = resultat;
                    } else {
                        dernierCommande->suivant = copieCommande;
                        dernierCommande = copieCommande;
                    }

                } else {
                    printf("Le numéro de l'item choisi est invalide. Veuillez réessayer.\n");
                }

            }
            retourMenu = 0;
        } else {
            printf("Le numéro du repas choisi est invalide. Veuillez réessayer.\n");
        }

        if (choixRepas == -1) {
            retourMenu = 0;

        }

    }


    return resultat;
}



void afficherCommande(commandeItem *commandes, int nombreCommandes) {


    if (nombreCommandes == 0) {
        printf("\nAucun article commandé pour le moment.\n");
            return;
    }

    printf("\n Votre commande en cours :\n");


    for (int i = 0; i < nombreCommandes; i++) {
        printf("%s x %d, Prix unitaire: %.2f$\n", commandes[i].titre, commandes[i].quantite, commandes[i].prix);
    }

}



void payer(commandeItem *commandes, int nombreCommandes, char *aNomFichier) {
    float total = 0.0;
    float taxe = 0.15;
    float totalAvecTaxe;
    FILE *lFichier = fopen(aNomFichier, "w");


    for (int i = 0; i < nombreCommandes; i++) {
        fprintf(lFichier, "Item: %s, Quantité: %d, Prix unitaire (avant taxes): %.2f$\n",
                                  commandes[i].titre, commandes[i].quantite, commandes[i].prix);
        total += commandes[i].prix * commandes[i].quantite;
    }

    for (int i = 0; i < nombreCommandes; i++) {
            printf(" \n%s x %d, Prix unitaire: %.2f$\n", commandes[i].titre, commandes[i].quantite,    commandes[i].prix);


        }


    fprintf(lFichier, "Montant total (sans taxe) : %.2f$\n", total);
     fprintf(lFichier, "Montant total (avec taxe) : %.2f$\n", totalAvecTaxe);

    totalAvecTaxe = total * (1 + taxe);

    printf("Montant total (avec taxe) : %.2f\n", totalAvecTaxe);


    fclose(lFichier);




}


void effacerCommande (commandeItem *commandes){
    commandeItem *actuel = commandes;
    commandeItem *suivant;

    while(actuel != NULL){
            suivant = actuel->suivant;
            free(actuel);
            actuel = suivant;

    }
}









