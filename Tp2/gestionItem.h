#ifndef GESTIONITEM_H
#define GESTIONITEM_H
#include "structureRepas.h"



void chargerMenu(menu *aMenu, char *aNomFichier);
void afficherMenu(menu aMenu);
commandeItem *commanderItems(menu aMenu, int *nombreCommandes);
void afficherCommande(commandeItem *commandes, int nombreCommandes);
void payer(commandeItem *commandes, int nombreCommandes, char *aNomFichier);
void effacerCommande (commandeItem *commandes);



#endif // GESTIONITEM_H
