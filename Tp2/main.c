#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structureRepas.h"
#include "gestionItem.h"

int main()
{
    menu lMenu;
    char *lNomFichier = "menu.txt";
    char *lFacture = "facture.txt";
    char entree[10];
    int choix =0;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    commandeItem *commandes = NULL;
    int nombreCommandes = 0;


    do {
        printf("1) commander un item\n");
        printf("2) afficher la commande\n");
        printf("3) payer\n");
        printf("0) sortir\n");
        printf("votre choix: ");
        fgets(entree, sizeof entree, stdin);
        choix= atoi(entree);

        if(choix == 1) {
        chargerMenu(&lMenu, lNomFichier);
        commandes = commanderItems(lMenu, &nombreCommandes);
        fgets(entree, sizeof entree, stdin);
        } else if(choix == 2) {
        afficherCommande(commandes, nombreCommandes);

        } else if(choix == 3) {
        payer(commandes,nombreCommandes, lFacture);
        effacerCommande (commandes);
        }
    } while(choix != 0);

}
